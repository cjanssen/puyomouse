function love.load()
    -- init random
    math.randomseed( os.time() )
    math.random()
    math.random()
    math.random()
    -- end init random

    love.filesystem.setIdentity( "puyomouse" )

    love.filesystem.load('timers.lua')()
    love.filesystem.load('vector.lua')()
    love.filesystem.load('globals.lua')()
    love.filesystem.load('graphics.lua')()
    love.filesystem.load('board.lua')()
    love.filesystem.load('blockqueue.lua')()
    love.filesystem.load('shooter.lua')()
    love.filesystem.load('colorspace.lua')()
    love.filesystem.load('particles.lua')()
    love.filesystem.load('score.lua')()

    -- love.mouse.setVisible(true)
    OperatingSystem = love.system.getOS()
    if OperatingSystem == "Android" or OperatingSystem == "iOS" then
        MobileOS = true
    else
        MobileOS = false
    end


    screenSize = Vector(240,426)
    -- scaledScreenSize = Vector(576,1024)
    scaledScreenSize = screenSize * 2
    local w,h = love.window.getMode()
    if not Vector(w,h):isEq(scaledScreenSize) then
        love.window.setMode(scaledScreenSize:unpack())
        -- love.window.setMode(scaledScreenSize.x, scaledScreenSize.y, {resizable=true})
    end

    if MobileOS then
        love.window.setFullscreen(true)
    end

    love.graphics.setDefaultFilter( "nearest", "nearest")

    introScale = 0.25
    introCanvas = love.graphics.newCanvas(screenSize:unpack())
    -- cheap blur by zooming
    introCanvas:setFilter("linear","linear")

    Board.init()
    Graphics.init()
    BlockQueue.init()
    Shooter.init()
    Particles.init()
    Score.init()

    resetGame()
end

function resetGame()
    Timers.cancelAll()
    Graphics.reset()
    Board.reset()
    BlockQueue.reset()
    Shooter.reset()
    Particles.reset()
    Score.reset()
    introScale = 0.25
    setGameState("intro")
end

function setGameState(newState)
    gameState = newState
    if newState == "intro" then
        local introTime = 1
        Timers.create(introTime)
        :withUpdate(function(elapsed)
            Graphics.introPercent = elapsed/introTime
        end)
        :andThen(function()
            Graphics.introPercent = 1
        end)
        :start()

    elseif newState == "playing" then
        Graphics.show_help = true
    elseif newState == "gameover" then
        local gameOverAnimDelay = 1.5
        local function setFrac(frac)
            Graphics.desatShader:send("amount",frac)
            Graphics.gameover_opacity = frac
        end

        gameOverAnim = Timers.create(gameOverAnimDelay)
            :prepare(function()
                setFrac(0)
            end)
            :withUpdate(function(elapsed)
                setFrac(elapsed/gameOverAnimDelay)
            end)
            :andThen(function()
                setFrac(1)
            end)
            :thenWait(2)
            :start()
    end
end

function love.update(dt)
    -- ignore glitches
    if dt > 0.1 then return end
    Timers.update(dt)
    Board.update(dt)
    BlockQueue.update(dt)
    Shooter.update(dt)
    Particles.update(dt)
    Score.update(dt)
    Graphics.update(dt)
end

function getScale()
    local realScreenSize = Vector(love.graphics.getWidth(), love.graphics.getHeight())
    -- preserve aspect ratio, constrain by x
    local sc = realScreenSize.x/screenSize.x
    return Vector(sc,sc)
end

function love.draw()
    love.graphics.push()
    if gameState == "intro" then
        love.graphics.scale(introScale,introScale)
        love.graphics.setCanvas(introCanvas)
    else
        love.graphics.scale(getScale():unpack())
    end

    if gameState == "gameover" then
        love.graphics.setShader(Graphics.desatShader)
    end

    Graphics.drawBg()
    Shooter.draw()
    Board.draw()
    BlockQueue.draw()
    Particles.draw()
    Score.draw()

    if gameState == "intro" then
        love.graphics.setCanvas()
        love.graphics.scale((getScale()/introScale):unpack())
        love.graphics.draw(introCanvas,0,0,0,1/introScale,1/introScale)
    end

    love.graphics.setShader()
    Graphics.drawFg()

    love.graphics.pop()
end

function love.keypressed(key)
    if key == "escape" then
        love.event.push("quit")
        return
    end

    -- if key == "space" then
    --     Board.spawnRow()
    --     Board.checkGameOver()
    -- end
end

function inButton(x,y)
    local sc = getScale()
    return x/sc.x >= Graphics.button_TL.x and y/sc.y >= Graphics.button_TL.y
end

function love.mousepressed(x,y,button)
    local sc = getScale()
    if gameState == "intro" and Graphics.introPercent >= 1 and
        inButton(x,y) then
        Graphics.introPercent = 0
        local outroTime = 0.65
        outroAnim = Timers.create(outroTime)
            :withUpdate(function(elapsed)
                introScale = 0.75 * elapsed/outroTime + 0.25
            end)
            :andThen(function()
                setGameState("playing")
            end)
            :start()
    end

    if gameState == "gameover" and gameOverAnim and not gameOverAnim:isRunning() and
        inButton(x,y) then
        resetGame()
    end
end