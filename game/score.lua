Score = Score or {}

function Score.init()
    Score.filename = "hiscore.dat"
    Score.score = 0
    Score.target = 0
    Score.desired_speed = 1000
    Score.speed = 1000 -- points per second

    Score.labels = {}
    Score.pending = {}
    Score.running = 0
    Score.hiscore = 0

    Score.loadHi()
end

function Score.reset()
    Score.init()
end

function Score.loadHi()
    if love.filesystem.exists(Score.filename) then
        local hiscore, size = love.filesystem.read(Score.filename)
        if size > 0 then
            Score.hiscore = tonumber(hiscore) or Score.hiscore
        end
    end
end

function Score.saveHi()
    if Score.target > Score.score then
        Score.score = Score.target
    end
    if Score.score > Score.hiscore then
        Score.hiscore = Score.score
        local tosave = tostring(Score.hiscore)
        love.filesystem.write( Score.filename, tosave, tosave:len() )
    end
end

function Score.addForBlock(block, seqNr)
    local x,y = block.x*0.5+block.ix, block.y*0.5+block.iy
    local xx, yy = (Board.orig + Vector(x-1,y-1) ^ Board.cellSize):unpack()
    Score.add(Score.calcForBlock(block, seqNr), Vector(xx,yy))
end

function Score.calcForBlock(block, seqNr)
    return math.floor(0.5 + math.pow(block.x,2) * math.pow(block.y,2) * (seqNr+1)) * 10
end

function Score.add(amount, pos)
    -- decelerate if necessary
    local eta = (Score.target - Score.score + amount) / Score.speed
    if eta < 0.25 then
        Score.speed = math.min(Score.desired_speed, (Score.target - Score.score + amount) / 0.25)
    elseif eta > 1 then
        Score.speed = math.max(Score.desired_speed, (Score.target - Score.score + amount) / 1)
    end

    Score.target = Score.target + amount

    local displayTime = 0.65

    local label = {
        txt = "+"..amount,
        opacity = 1,
        pos = pos:copy()
    }
    table.insert(Score.labels, label)

    -- create animation
    local newAnim = Timers.create(displayTime)
        :prepare(function()
            Score.running = Score.running + 1
        end)
        :withUpdate(function(elapsed)
            label.opacity = 1 - elapsed/displayTime
        end):andThen(function()
            label.opacity = 0
        end)
        :finally(function()
            if #Score.pending > 0 then
                local next_anim = Score.pending[1]
                table.remove(Score.pending, 1)
                next_anim:start()
            end
            Score.running = Score.running - 1
        end)

    if Score.running == 0 then
        newAnim:start()
    else
        table.insert(Score.pending, newAnim)
    end
end

function Score.update(dt)
    if Score.score < Score.target then
        Score.score = math.floor(math.min(Score.score + Score.speed*dt, Score.target))
    end
end

function Score.draw()
    love.graphics.setColor(255,255,255)
    local x0,y0 = 35, 392
    local txt = "Score: "
    -- local w0,h0 = Graphics.font:getWidth(txt), Graphics.font:getHeight(txt)
    Graphics.print(txt..Score.score, x0,y0)

    local any = false
    for i=#Score.labels,1,-1 do
        local label = Score.labels[i]
        if label.opacity > 0 then
            any = true
            love.graphics.setColor(255,255,255,label.opacity * 255)
            -- Graphics.print(label.txt, x0+w0*2, y0 - (label.h+1)*h0)
            Graphics.printc(label.txt, label.pos.x, label.pos.y)
        end
    end

    if #Score.labels > 0 and not any then
        Score.labels = {}
    end
end