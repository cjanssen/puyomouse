Board = Board or {}

function Board.init()

    -- constants
    Board.size=Vector(12,24)
    Board.viewSize=Vector(192,192/Board.size.x*Board.size.y)
    local ww = Board.viewSize.x / Board.size.x;
    Board.cellSize = Vector(ww,ww)
    Board.orig = Vector((screenSize.x - Board.viewSize.x)*0.5 ,0)
    Board.orig = Board.orig + Vector(0,50)

    Board.startingRows = 8
    Board.rowLimit = 18
    Board.colorCount = 6
    Board.maxInc = 4

    Board.vibrationTimer = 0

    Board.initGraphics()
end

function Board.reset()
    Board.level = 1
    Board.shots_left = Board.shotsPerLevel(Board.level)
    Board.respawn()
    Board.launchAnimation()
end

function Board.initGraphics()
    do
        -- blocks
        Board.sheets = {}
        for color = 1,6 do
            local img = love.graphics.newImage("img/sheet_blob"..color..".png")
            local w,h = img:getWidth(), img:getHeight()
            Board.sheets[color] = {}
            Board.sheets[color].sprites ={}
            Board.sheets[color].batch = love.graphics.newSpriteBatch(img, 12*24, "static")
            -- Board.sheets[color].faces = {}
            local iy= 0
            local ndx = 1
            for inc=4,1,-1 do
                for iiy = 0,3 do
                    Board.sheets[color].sprites[ndx] = {}
                    for iix = 0,3 do
                        table.insert(Board.sheets[color].sprites[ndx],
                            love.graphics.newQuad(iix*16*4, (iy + iiy * inc) * 16 + 1, 16*4 - 1, 16*inc-1, w,h))
                    end
                    ndx = ndx + 1
                end
                iy = iy + inc * 4
            end

            -- one face per color
            Board.sheets[color].faces = {
                love.graphics.newQuad(4*4*16, (40 - 1)*16, 16, 16, w, h),
                love.graphics.newQuad(4*4*16+16, (40 - 1)*16, 16, 16, w, h)
            }
        end
    end

    do
        -- pop
        local img = love.graphics.newImage("img/sheet_pop.png")
        local w,h = img:getWidth(), img:getHeight()
        Board.pop_sheet = {}
        Board.pop_sheet.sprites = {}
        Board.pop_sheet.batch = love.graphics.newSpriteBatch(img, 12*24, "static")

        for iy=0,3 do
            Board.pop_sheet.sprites[iy+1] = {}
            for ix=0,7 do
                table.insert(Board.pop_sheet.sprites[iy+1],
                    love.graphics.newQuad(ix*32, (iy+4)*32, 32, 32, w, h))
            end
        end
    end
end

function Board.launchAnimation()
    Board.anim = {
        frame = 1,
        frameTime = 0.045,
        frameCount = 4,
        count = 0,
        iterations = 4
    }

    CreateRepeatAnimation(
        Board.anim.frameCount,
        Board.anim.frameTime,
        Board.setDirtyAnim,
        Board.anim):start()

    Board.popList = {}
end

local size_translation = {
    [4] = 0,
    [3] = 256,
    [2] = 448,
    [1] = 576
}

function Board.sizeToIndex(size)
    local x_comp = (4-size.x)
    local y_comp = (4-size.y)
    return 1 + y_comp*4 + x_comp

end

function Board.clear()
    Board.map = {}
    for x=1,Board.size.x do
        Board.map[x] = {}
        for y=1,Board.size.y do
            Board.map[x][y] = 0
        end
    end
    Board.randColorPoints = {}
end

function Board.respawn()
    local realColorCount = Board.colorCount
    Board.colorCount = 3

    -- populate
    Board.clear()
    for x=1,Board.size.x do
        local fx = (x-1)/Board.size.x
        for y=1,Board.startingRows do
            local fy = (y-1)/Board.startingRows
            Board.map[x][y] = Vector(1,1, { color = Board.getDistColor(Vector(fx,fy)), ix = x, iy = y })
        end
    end

    -- link clumps together
    Board.initialClump(2, true)
    Board.buildLinkMap()
    Board.mergeClumps()

    -- recolorize
    Board.colorCount = realColorCount
    for x=1,Board.size.x do
        for y=1,Board.size.y do
            local block = Board.map[x][y]
            if block ~= 0 then
                block.color = Board.getRandomColor()
            end
        end
    end

    Board.mergeClumps()

    -- draw
    Board.setDirty()
end

function Board.initialClump(mi, horv)
    local maxInc = mi or Board.maxInc

    local function arrangeVertical()
        -- vertical
        for x=1,Board.size.x do
            for y=1,Board.size.y do
                if Board.map[x][y] ~= 0 then
                    local cell = Board.map[x][y]
                    local inc = 1

                    while y+inc <= Board.size.y and
                        inc < maxInc and
                        Board.map[x][y+inc] ~= 0 and
                        Board.map[x][y+inc].x == cell.x and
                        Board.map[x][y+inc].color == cell.color do

                            Board.map[x][y+inc] = 0
                            inc = inc+1
                            cell.y = inc
                    end
                end
            end
        end
    end

    local function arrangeHorizontal()
        -- horizontal
        for x=1,Board.size.x do
            for y=Board.size.y,1,-1 do
                if Board.map[x][y] ~= 0 then
                    local inc = 1
                    local cell = Board.map[x][y]
                    while x+inc <= Board.size.x and
                        inc < maxInc and
                        Board.map[x+inc][y] ~= 0 and
                        Board.map[x+inc][y].y == cell.y and
                        Board.map[x+inc][y].color == cell.color do
                            Board.map[x+inc][y] = 0
                            inc = inc + 1
                            cell.x = inc
                    end

                end
            end
        end
    end

    if horv then
        arrangeHorizontal()
        arrangeVertical()
    else
        arrangeVertical()
        arrangeHorizontal()
    end
end

function Board.mergeClumps()
    for x=1,Board.size.x do
        for y=1,Board.size.y do
            local cell = Board.map[x][y]
            if cell ~= 0 then
                local rebuild = false
                Board.map[cell.ix][cell.iy] = 0
                cell,rebuild = Board.addCell_connectVertical(cell,rebuild)
                cell,rebuild = Board.addCell_connectHorizontal(cell,rebuild)
                Board.map[cell.ix][cell.iy] = cell
                if rebuild then
                    Board.buildLinkMap()
                end
            end
        end
    end
end

function Board.cleanNormalMap()
    for x=1,Board.size.x do
        for y=1,Board.size.y do
            local cell = Board.map[x][y]
            if cell ~= 0 then
                for ix=1,cell.x do
                    for iy = 1,cell.y do
                        if iy~=1 or ix~=1 then
                            Board.map[cell.x+ix-1][cell.y+iy-1] = 0
                        end
                    end
                end
            end
        end
    end
end

function Board.buildLinkMap()
    -- first clean
    Board.linkMap = {}
    for x=1,Board.size.x do
        Board.linkMap[x] = {}
        for y=1,Board.size.y do
            Board.linkMap[x][y] = 0
        end
    end

    -- now link
    for x=1,Board.size.x do
        for y=1,Board.size.y do
            if Board.map[x][y] ~= 0 then
                for ix=1,Board.map[x][y].x do
                    for iy=1,Board.map[x][y].y do
                        Board.linkMap[x+ix-1][y+iy-1] = Board.map[x][y]
                    end
                end
            end
        end
    end
end

function Board.shotsPerLevel(level)
    return math.max(math.ceil(10 - level/3), 2)
end

function Board.updateShootCount()
    Board.shots_left = Board.shots_left - 1
    if Board.shots_left <= 0 then
        Board.level = Board.level + 1
        Board.rowAnimation = Timers.create():andThen(Board.spawnRow):andThen(function()
            Board.rowAnimation = nil
            Board.shots_left = Board.shotsPerLevel(Board.level)
            Board.checkGameOver()
        end):start()
    end
end

function Board.spawnRow()
    -- shift all cells and remove last row
    for x=1,Board.size.x do
        for y=1,Board.size.y-1 do
            local cell = Board.map[x][y]
            if cell ~= 0 then
                cell.iy = cell.iy + 1
            end
        end
        table.remove(Board.map[x], Board.size.y)
    end

    -- insert new row of cells
    local lastColor
    for x=1,Board.size.x do
        -- choose color based on neighbours
        local colorDecision
        if Board.linkMap[x][1] ~= 0 and math.random() < 0.7 then
            colorDecision = Board.linkMap[x][1].color
        elseif lastColor and math.random() < 0.7 then
            colorDecision = lastColor
        else
            colorDecision = Board.getRandomColor()
        end
        lastColor = colorDecision
        table.insert(Board.map[x], 1,  Vector(1,1, { color = colorDecision, ix = x, iy = 1 }))
    end

    -- Board.buildLinkMap()
    -- Board.initialClump(Board.maxInc, false)
    Board.buildLinkMap()
    Board.mergeClumps()
    Board.mergeClumps()
    Board.buildLinkMap()
end

function Board.checkGameOver()
    for x=1,Board.size.x do
        for y=Board.rowLimit,Board.size.y do
            if Board.linkMap[x][y] ~= 0 then
                -- game over man!
                Score.saveHi()
                setGameState("gameover")
            end
        end
    end
end

function Board.mouseToBoard(pos)
    return ((pos - Board.orig) / Board.cellSize):floor() + Vector(1,1)
end

function Board.getCell(coord)
    if coord.x < 1 or coord.y < 1 or coord.x > Board.size.x or coord.y > Board.size.y then return 0 end
    return Board.linkMap[coord.x][coord.y]
end

function Board.eraseCell(coord, seqNr)
    seqNr = seqNr or 1
    local cell = Board.getCell(coord)
    if cell ~= 0 then
        Board.map[cell.ix][cell.iy] = 0
        for x=1,cell.x do
            for y=1,cell.y do
                Board.linkMap[cell.ix + x - 1][cell.iy + y - 1] = 0
            end
        end
        Board.popAnimation(cell)
        Score.addForBlock(cell, seqNr)
        Timers.setTimeout(function() Board.checkUnder(cell, seqNr) end, 0.25)
        if Board.rowAnimation then
            Board.rowAnimation:withTimeout(Board.rowAnimation.timeout + 0.30)
        end
    end
    Board.setDirty()
end

function Board.checkUnder(cell, seqNr)
    local falling_cells = {}
    for x=cell.ix,cell.ix+cell.x-1 do
        local candidate = Board.getCell(Vector(x, cell.iy+cell.y))
        if candidate ~= 0 then
            -- check if candidate has anyone above
            local dead = true
            for xx=candidate.ix,candidate.ix+candidate.x-1 do
                if Board.getCell(Vector(xx, candidate.iy-1)) ~= 0 then
                    dead = false
                    break
                end
            end
            if dead then
                falling_cells[candidate] = true
            end
        end
    end

    for fc,_ in pairs(falling_cells) do
        Board.eraseCell(Vector(fc.ix, fc.iy), seqNr + 1)
    end
end

function Board.addCell_connectHorizontal(newCell, changed)
    if newCell.ix > 1 then
        -- left
        local connection = Board.linkMap[newCell.ix-1][newCell.iy]
        if connection ~= 0 and connection.color == newCell.color and
            connection.y == newCell.y and connection.iy == newCell.iy and
            connection.x+newCell.x <= Board.maxInc then

            connection.x = connection.x + newCell.x
            Board.map[newCell.ix][newCell.iy] = 0
            newCell = connection
            changed = true
        end
    end

    if newCell.ix + newCell.x <= Board.size.x then
        local connection = Board.linkMap[newCell.ix+newCell.x][newCell.iy]
        if connection ~= 0  and connection.color == newCell.color and connection.y == newCell.y
            and connection.iy == newCell.iy and connection.x+newCell.x <= Board.maxInc then
            newCell.x = connection.x + newCell.x
            Board.map[connection.ix][connection.iy] = 0
            changed = true
        end
    end
    return newCell, changed
end

function Board.addCell_connectVertical(newCell, changed)

    if newCell.iy > 1 then
        -- up
        local connection = Board.linkMap[newCell.ix][newCell.iy-1]
        if connection ~= 0  and connection.color == newCell.color and connection.x == newCell.x
            and connection.ix == newCell.ix and connection.y+newCell.y <= Board.maxInc then
            connection.y = connection.y + newCell.y
            Board.map[newCell.ix][newCell.iy] = 0
            newCell = connection
            changed = true
        end


        -- down
        if newCell.iy + newCell.y <= Board.size.y then
            local connection = Board.linkMap[newCell.ix][newCell.iy+newCell.y]
            if connection ~= 0  and connection.color == newCell.color and connection.x == newCell.x
                and connection.ix == newCell.ix and connection.y+newCell.y <= Board.maxInc then
                newCell.y = connection.y + newCell.y
                Board.map[connection.ix][connection.iy] = 0
                changed = true
            end
        end

    end
    return newCell, changed

end

function Board.addCell(coord, color)
    if coord.x < 1 or coord.y < 1 or coord.x > Board.size.x or coord.y > Board.size.y then return end
    if Board.getCell(coord) ~= 0 then return end

    local newCell = Vector(1,1,{ color = color, ix = coord.x, iy = coord.y })
    Board.map[coord.x][coord.y] = newCell
    Board.linkMap[coord.x][coord.y] = newCell

    local anychanged = false
    local changed = false
    repeat
        changed = false
        newCell, changed = Board.addCell_connectHorizontal(newCell, changed)
        newCell, changed = Board.addCell_connectVertical(newCell, changed)
        Board.map[newCell.ix][newCell.iy] = newCell
        anychanged = anychanged or changed
    until not changed

    if anychanged then
        -- Board.cleanNormalMap()
        Board.buildLinkMap()
    end

    Board.animateBlock(newCell)

    Board.setDirty()

    if coord.y >= Board.rowLimit then
        Board.checkGameOver()
    end

    -- count time until animation done if new row is to be spawned
    if Board.rowAnimation then
        Board.rowAnimation:withTimeout(Board.rowAnimation.timeout +
            Board.anim.frameCount * Board.anim.frameTime * Board.anim.iterations)
    end
end

function Board.animateBlock(block)
    block.frameOfs = (block.ix + block.iy)
    block.animated = true
    Board.anim.count = Board.anim.count + 1

    Timers.create(Board.anim.frameCount * Board.anim.frameTime * Board.anim.iterations)
        :andThen(function()
            block.animated = false
            Board.anim.count = Board.anim.count - 1
            -- force redraw (we might have stopped at a non-first frame)
            -- and we want the idle frame to be the first
            Board.setDirty()
            end):start()

end

function Board.popAnimation(block)

    local frameTime = 0.040
    local frameCount = 8
    local T = frameTime * (frameCount + 2)

    local pop = {}
    for ix = 1,block.x do
        for iy = 1,block.y do
            table.insert(pop, Vector(
                ix - 1 + block.ix,
                iy - 1 + block.iy,
                {
                    fOfs = -math.random()*frameTime*2,
                    fNdx = math.random(4),
                    frame = 0
                })
            )
        end
        pop.dead = false
    end

    table.insert(Board.popList, pop)

    Timers.create(T)
        :withUpdate(function(elapsed)
            for i = 1,#pop do
                local oldframe = pop[i].frame
                pop[i].frame = math.floor((elapsed + pop[i].fOfs)/frameTime) + 1
                if oldframe ~= pop[i].frame then
                    Board.setPopDirty()
                end
            end
        end)
        :andThen(function()
            pop.dead = true
            Board.setPopDirty()
         end)
        :start()

    local center = Vector(block.ix + block.x*0.5, block.iy + block.y*0.5) ^ Board.cellSize + Board.orig
    for x=1,block.x do
        for y=1,block.y do
            local pos = Vector(block.ix+x-1, block.iy+y-1) ^ Board.cellSize + Board.orig
            local diff = pos - center
            for i=1,10 do
                Particles.spawn(pos, diff * 5)
            end
        end
    end
end

function Board.setPopDirty()
    Board._pop_dirty = true
end

function Board.getRandomColor()
    return math.random(Board.colorCount)
end

function Board.getDistColor(pos)
    local rcp = Board.randColorPoints
    if #rcp == 0 then
        -- initialize random color points
        for i=1,Board.colorCount do
            for j=1,3 do
                table.insert(rcp, Vector(math.random(), math.random(), { ndx = i }))
            end
        end
    end

    -- find closest point
    local probs = {}
    local max = 1
    for i=1,#rcp do
        probs[i] = (rcp[i]-pos):mod()
        if probs[i] < probs[max] then max = i end
    end

    return rcp[max].ndx
end

function Board.setDirty()
    Board._dirty = true
end

function Board.setDirtyAnim()
    if Board.anim.count > 0 then
        Board.setDirty()
    end
end

function Board.updateBlockAnimation(dt)
    local function getNewFDelay()
        return math.random()*5 + 1
    end

    local function getNewTDelay()
        return math.random()*6 + 2
    end

    for x=1,Board.size.x do
        for y=1,Board.size.y do
            local block = Board.map[x][y]
            if block ~= 0 then
                do
                    -- face animation
                    block.ftimer = (block.ftimer or 0) + dt
                    block.fdelay = block.fdelay or getNewFDelay()*0.3
                    block.frame = block.frame or 1
                    if block.ftimer >= block.fdelay then
                        block.ftimer = 0
                        block.frame = 3 - block.frame
                        if block.frame == 1 then
                            block.fdelay = getNewFDelay()
                        else
                            block.fdelay = 0.2
                        end
                        Board._dirty = true
                    end

                    -- tremble animation
                    block.ttimer = (block.ttimer or 0)+dt
                    block.tdelay = block.tdelay or getNewTDelay()*0.3
                    block.tremble = block.tremble or Vector()
                    if block.ttimer >= block.tdelay then
                        block.ttimer = 0
                        block.tdelay = getNewTDelay()
                        Timers.create(0.2):withUpdate(function()
                            block.tremble = (Vector(math.random()-0.5, math.random()-0.5) * 2):floor()
                            Board._dirty = true
                        end):andThen(function()
                            block.tremble = Vector()
                            Board._dirty = true
                        end)
                        :start()
                    end
                end
            end
        end
    end
end

function Board.update(dt)
    if Board.shots_left <= 2 then
        Board.vibrationTimer = Board.vibrationTimer + dt
    else
        Board.vibrationTimer = 0
    end

    Board.updateBlockAnimation(dt)

    if Board._dirty then
        Board._dirty = false

        for i=1,#Board.sheets do
            Board.sheets[i].batch:clear()
        end

        love.graphics.setColor(255,255,255)
        for x=1,Board.size.x do
            for y=1,Board.size.y do
                local block = Board.map[x][y]
                if block ~= 0 then
                    -- block display
                    local sh = Board.sheets[block.color]
                    local ndx = Board.sizeToIndex(Board.map[x][y])
                    local ifr = 1
                    if block.animated then
                        ifr = ((Board.anim.frame + block.frameOfs) % Board.anim.frameCount) + 1
                    end
                    local xx, yy = (Board.orig + Vector(x-1,y-1) ^ Board.cellSize):unpack()
                    sh.batch:add(sh.sprites[ndx][ifr], xx+block.tremble.x, yy+block.tremble.y)

                    -- face display
                    local xx, yy = (Board.orig +
                        Vector(1,0) +
                        Vector(
                            block.ix + block.x*0.5 - 1.5,
                            block.iy + block.y*0.5 - 1.5)
                         ^ Board.cellSize):unpack()
                    sh.batch:add(sh.faces[block.frame], xx, yy)
                end
            end
        end
    end

    if Board._pop_dirty then
        Board._pop_dirty = false

        Board.pop_sheet.batch:clear()
        for i=#Board.popList,1,-1 do
            local pop = Board.popList[i]
            if pop.dead then
                table.remove(Board.popList, i)
            else
                for j=1,#pop do
                    if pop[j].frame >=1 and pop[j].frame <= 8 then
                        local sprite = Board.pop_sheet.sprites[pop[j].fNdx][pop[j].frame]
                        local v  = (Board.orig + (pop[j] - Vector(1,1)) ^ Board.cellSize) + Vector(8,8)
                        Board.pop_sheet.batch:add(sprite,
                            v.x, v.y, 0,1,1,16,16)
                    end
                end
            end
        end
    end

end

function Board.draw()
    love.graphics.setColor(255,255,255)
    local disp = 0
    if Board.shots_left == 2 then
        disp = math.sign(math.sin(Board.vibrationTimer*10))*0.5 + 0.5
    elseif Board.shots_left <= 1 then
        disp = math.sign(math.sin(Board.vibrationTimer*40))*0.5 + 0.5
        -- disp = math.floor(math.sin(Board.vibrationTimer*40)*0.5+0.5)
    end
    for i=1,#Board.sheets do
        love.graphics.draw(Board.sheets[i].batch, disp)
    end

    love.graphics.draw(Board.pop_sheet.batch)
end