Colors = Colors or {}
Colors.rgb = {
    [0] = {0,0,0},
    {255,0,0},
    {0,255,0},
    {0,0,255},
    {255,255,0},
    {255,0,255},
    {0,255,255},
}

function CreateRepeatAnimation(frameCount, frameTime, triggerFunc, frameContainer)
    local a = frameContainer
    a.frame = 1
    return
        Timers.create(frameCount * frameTime)
        :prepare(function()
            a.frame = 1
            triggerFunc()
        end)
        :withUpdate(function(elapsed)
            local old = a.frame
            a.frame = (math.floor(elapsed / frameTime) % frameCount) + 1
            if a.frame ~= old then
                triggerFunc()
            end
        end)
        :thenRestart()
end

function CreateSpriteAnimation(frameCount, frameTime, triggerFunc, frameContainer)
    local a = frameContainer
    a.frame = 1
    return
        Timers.create(frameCount * frameTime)
        :prepare(function()
            a.frame = 1
            triggerFunc()
        end)
        :withUpdate(function(elapsed)
            local old = a.frame
            a.frame = (math.floor(elapsed / frameTime) % frameCount) + 1
            if a.frame ~= old then
                triggerFunc()
            end
        end)
end