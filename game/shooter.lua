Shooter = Shooter or {}

function Shooter.init()
    Shooter.ipos = Vector(6,0)
    Shooter.topleft = Board.orig + Vector(0, 310)
    Shooter.shoot_wait_delay = 12
    Shooter.initGraphics()
end

function Shooter.reset()
    Shooter.ipos = Vector(6,0)
    Shooter.available = true
    Shooter.charge_visible = true
    Shooter.shoot_wait_timer = Shooter.shoot_wait_delay
    Shooter.launchAnim()
end

function Shooter.initGraphics()
    do
        -- shots
        Shooter.aimline = { sheet = {} }
        local img = love.graphics.newImage("img/aimlines.png")
        local w,h = img:getWidth(), img:getHeight()
        Shooter.aimline.sheet.sprites = {}
        Shooter.aimline.sheet.batch = love.graphics.newSpriteBatch(img, 12*24*8, "static")
        for iy = 9,15 do
            local i = iy-9
            Shooter.aimline.sheet.sprites[i] = {}
            for ix=0,15 do
                table.insert(Shooter.aimline.sheet.sprites[i],
                    love.graphics.newQuad(ix*16,iy*16,15,16,w,h))
            end
        end
    end

    do
        -- mouse
        Shooter.charac = { sheet = {} }
        local img = love.graphics.newImage("img/sheet_mouse.png")
        local w,h = img:getWidth(), img:getHeight()
        Shooter.charac.sheet.sprites = {}
        Shooter.charac.sheet.batch = love.graphics.newSpriteBatch(img, 12, "static")
        Shooter.charac.states = { stand = 1 , walk = 2, shoot = 3 }
        Shooter.charac.frameCounts = { stand = 12, walk = 2, shoot = 6 }
        for st,i in pairs(Shooter.charac.states) do
            Shooter.charac.sheet.sprites[i] = {}
            local iy = (4-i)
            for ix=0,Shooter.charac.frameCounts[st] do
                table.insert(Shooter.charac.sheet.sprites[i],
                    love.graphics.newQuad(ix*64,iy*64,64,64,w,h))
            end
        end

        Shooter.charac.state = "stand"
        -- Shooter.charac.anim = { frameCount = 12 }
    end

    -- done
    Shooter.setDirty()
    Shooter.setCharacDirty()
end

function Shooter.launchAnim()
    Shooter.aimline.frameTime = 0.110
    CreateRepeatAnimation(16, Shooter.aimline.frameTime, Shooter.setDirty, Shooter.aimline):start()
    Shooter.charac.frameTime = 0.100
    Shooter.charac.repeatAnim = CreateRepeatAnimation(12, Shooter.charac.frameTime, Shooter.setCharacDirty, Shooter.charac):start()

    Shooter.walk = {}
    Shooter.walk.pos = Shooter.ipos.x * Board.cellSize.x + Shooter.topleft.x
    Shooter.walk.wobble = 0
    Shooter.walk.orientation = 1
end

function Shooter.setDirty()
    Shooter._dirty = true
end

function Shooter.setCharacDirty()
    Shooter._charac_dirty = true
end

function Shooter.update(dt)
    if gameState == "playing" then
        Shooter.updateShoot(dt)
        Shooter.updateDeadline(dt)
    end
    Shooter.updateAimlineAnim(dt)
    Shooter.updateWalkAnim(dt)
    Shooter.updateCharacAnim(dt)
end

function Shooter.updateShoot(dt)
    -- mouse status
    local mp =  Vector(love.mouse.getX(), love.mouse.getY()) / getScale()
    local pressed = love.mouse.isDown(1,2,3)
    local pressAction = not Shooter.pressed and pressed
    local releaseAction = Shooter.pressed and not pressed
    Shooter.pressed = pressed

    local decisionLineTop = 324
    local decisionLineBottom = 332

    local aboveLine = mp.y < decisionLineTop
    local belowLine = mp.y > decisionLineBottom
    local fatFingerBorder = 8
    local aimX = Shooter.topleft.x + (Shooter.ipos.x - 1) * Board.cellSize.x


    -- drag the aim line
    if aboveLine and not Shooter.aimline.drag and pressAction then
        if mp.x >= aimX - fatFingerBorder and mp.x <= aimX + 16 + fatFingerBorder then
            Shooter.aimline.drag = true
            Shooter.aimline.fetchdrag = false
        else
            Shooter.aimline.fetchdrag = true
        end
    elseif Shooter.aimline.fetchdrag and pressed then
        if mp.x >= aimX - fatFingerBorder and mp.x <= aimX + 16 + fatFingerBorder then
            Shooter.aimline.drag = true
            Shooter.aimline.fetchdrag = false
        end
    elseif Shooter.aimline.fetchdrag and not pressed then
        Shooter.aimline.fetchdrag = false
    elseif Shooter.aimline.drag and pressed then
        local dest = Board.mouseToBoard(mp)
        if dest.x>=1 and dest.x<=Board.size.x and dest.x ~= Shooter.ipos.x then
            if dest.x ~= Shooter.ipos.x then
                Graphics.show_help = false
            end
            Shooter.ipos.x = dest.x
        end

        Shooter.setDirty()
    elseif Shooter.aimline.drag and not pressed then
        Shooter.aimline.drag = false
    end

    -- shoot
    if belowLine and not Shooter.aimline.preparingShoot and pressAction and Shooter.canShoot() then
        Shooter.prepareShootAnim()
        Shooter.available = false
    elseif aboveLine and Shooter.aimline.preparingShoot and releaseAction then
        Shooter.aimline.preparingShoot = false
        Shooter.readyToShootAnim()
        Shooter.available = true
    elseif not aboveLine and Shooter.aimline.preparingShoot and releaseAction then
        Shooter.aimline.preparingShoot = false
        Shooter.available = true
        Shooter.cancelShootAnim()
    end
end

function Shooter.prepareShootAnim()
    Shooter.aimline.preparingShoot = true
    Shooter.charac.state = "shoot"
    Shooter.aimline.readyToShoot = false
    Shooter.charac.repeatAnim:cancel()
    CreateSpriteAnimation(3, Shooter.charac.frameTime, Shooter.setCharacDirty, Shooter.charac)
    :andThen(Shooter.readyToShootAnim)
    :start()
end

function Shooter.readyToShootAnim()
    if not Shooter.aimline.readyToShoot then
        -- declare that you can launch the actual shoot animation at any time
        Shooter.aimline.readyToShoot = true
    else
        -- oh, it's time! then launch it now
        Graphics.show_help = false
        Shooter.shoot()
    end
end

function Shooter.cancelShootAnim()
    Shooter.charac.state = "stand"
    Shooter.charac.repeatAnim:start()
end

function Shooter.updateAimlineAnim(dt)
    -- update aimline
    if Shooter._dirty then
        Shooter._dirty = false
        Shooter.aimline.sheet.batch:clear()
        do
            local x,y = (Shooter.topleft + (Shooter.ipos-Vector(1,1)) ^ Board.cellSize):unpack()
            x =  Shooter.walk.pos - 16
            local sprite = Shooter.aimline.sheet.sprites[BlockQueue.first()][Shooter.aimline.frame]
            Shooter.aimline.sheet.batch:setColor(255,255,255,255)
            for iy=0,17 do
                Shooter.aimline.sheet.batch:add(
                    sprite,
                    x, iy*16+Board.orig.y)
            end
        end
    end
end

function Shooter.updateWalkAnim(dt)
    -- prepare walk
    local destiPos = Shooter.ipos.x * Board.cellSize.x + Shooter.topleft.x
    if Shooter.walk.pos ~= destiPos and Shooter.charac.state ~= "shoot" then
        Shooter.setCharacDirty()
        local speed = 400
        local step = speed * dt
        if math.abs(Shooter.walk.pos - destiPos) <= step then
            Shooter.walk.pos = destiPos
            Shooter.charac.state = "stand"
            local T = 0.35
            local f = 5
            local a = 2
            if Shooter.wobbleAnim and Shooter.wobbleAnim:isRunning() then
                Shooter.wobbleAnim:cancel()
            end
            Shooter.wobbleAnim = Timers.create(T)
                :withUpdate(function(elapsed)
                    local frac = elapsed/T
                    Shooter.walk.wobble = math.sin(math.pi*2*frac*f)*a*(1-frac)
                    Shooter.setCharacDirty()
                end)
                :andThen(function()
                    Shooter.walk.wobble = 0
                    Shooter.setCharacDirty()
                end):start()

        else
            Shooter.walk.wobble = 0
            Shooter.charac.state = "walk"
            Shooter.walk.orientation = 1
            if destiPos < Shooter.walk.pos then
                Shooter.walk.orientation = -1
            end
            Shooter.walk.pos = Shooter.walk.pos + Shooter.walk.orientation * speed * dt
        end
    end
end

function Shooter.updateCharacAnim(dt)
    -- update charac
    if Shooter._charac_dirty then
        Shooter._charac_dirty = false
        Shooter.charac.sheet.batch:clear()
        local x,y = (Shooter.topleft + (Shooter.ipos-Vector(1,1)) ^ Board.cellSize):unpack()
        local fc = Shooter.charac.frameCounts[Shooter.charac.state]
        local fi = ((Shooter.charac.frame + fc-1) % fc) + 1
        local sprite = Shooter.charac.sheet.sprites[Shooter.charac.states[Shooter.charac.state]][fi]
        x = Shooter.walk.pos - 16 + 2*Shooter.walk.orientation
        Shooter.charac.sheet.batch:add(
            sprite,
            x + 8 + Shooter.walk.wobble, y - 24, 0, Shooter.walk.orientation, 1, 32, 0)
    end
end

function Shooter.updateDeadline(dt)
    Shooter.shoot_wait_timer = Shooter.shoot_wait_timer - dt
    local preptime = 3 * Shooter.charac.frameTime
    if Shooter.shoot_wait_timer <= preptime and not Shooter.aimline.preparingShoot and Shooter.canShoot() then
        Shooter.prepareShootAnim()
        Shooter.available = false
    end

    if Shooter.shoot_wait_timer <= 0 then
        Shooter.available = true
        Shooter.aimline.preparingShoot = false
        Shooter.shoot()
    end
end

function Shooter.shoot()
    if Shooter.shoot_block_timer and Shooter.shoot_block_timer:isRunning() then
        Shooter.charac.state="stand"
        Shooter.charac.repeatAnim:start()
        return
    end
    Shooter.shoot_block_timer = Timers.create(1.5):start()

    -- pang!
    -- local newColor = BlockQueue.iterate()
    Shooter.shoot_wait_timer = Shooter.shoot_wait_delay
    local newColor = BlockQueue.first()

    local function getYpos()
        for y=Board.size.y-1,1,-1 do
            if Board.getCell(Vector(Shooter.ipos.x, y)) ~= 0 then
                return y+1
            end
        end
        return 1
    end

    local dest = Vector(Shooter.ipos.x, getYpos())
    if newColor == 0 then
        dest.y = dest.y - 1
    end

    local function shoot_result()
        Board.updateShootCount()
        if newColor == 0 then
            -- rainbow
            Board.eraseCell(dest)
        else
            Board.addCell(dest, newColor)
        end
    end

    local epos = (Shooter.topleft + (Shooter.ipos-Vector(1,1)) ^ Board.cellSize)

    -- time for the animation:
    local shot_speed = 500 -- px/s
    local travel_distance = (Board.size.y - dest.y - 4) * 16
    local TotalTime = travel_distance / shot_speed

    -- particle trail (rainbow)
    local particleDelay = 0.013
    local particleTime = 0
    local lastTime = 0

    local shootAnim = Timers.create(TotalTime)
        :prepare(function()
            Shooter.charge_visible = false
            BlockQueue.block()
            Shooter.setDirty()
        end)
        :withUpdate(function(elapsed)
            local dt = elapsed - lastTime
            lastTime = elapsed
            local yy = epos.y - (elapsed/TotalTime) * travel_distance
            BlockQueue.sheet.batch_shoot:clear()
            if newColor == 0 then
                BlockQueue.sheet.batch_shoot:add(
                    BlockQueue.sheet.sprites.rainbow_shoot[BlockQueue.anim.frame],
                    epos.x, yy)
                particleTime = particleTime + dt
                while particleTime > particleDelay do
                    particleTime = particleTime - particleDelay
                    Particles.spawn(Vector(epos.x + 8, yy), VectorFromPolar(30, math.random()*math.pi))
                end
            else
                local frame = (BlockQueue.anim.frame%4)+1
                BlockQueue.sheet.batch_shoot:add(
                    BlockQueue.sheet.sprites.capsules[newColor][frame],
                    epos.x, yy)
            end
        end)
        :andThen(function()
            BlockQueue.sheet.batch_shoot:clear()
            -- Shooter.available = true
            -- Shooter.charge_visible = true
            shoot_result()
            Shooter.setDirty()
            BlockQueue.iterate()
            BlockQueue.consume()
            end)

    local mouseAnim = Timers.create(Shooter.charac.frameTime * 3)
        :prepare(function()
            Shooter.charac.state="shoot"
            -- Shooter.charac.repeatAnim:start()
            local framecontainer = { frame = 1 }
            local triggerfunc = function()
                Shooter.charac.frame = framecontainer.frame + 3
                Shooter.setCharacDirty()
            end
            CreateSpriteAnimation(3, Shooter.charac.frameTime, triggerfunc, framecontainer):start()
        end)
        :andThen(function()
            Shooter.charac.state="stand"
            Shooter.charac.repeatAnim:start()
        end)

    local finalAnim = Timers.create()
    finalAnim:hang(mouseAnim)
    -- finalAnim:thenWait(Shooter.charac.frameTime * 3):hang(shootAnim)
    finalAnim:hang(shootAnim)
    finalAnim:start()
end

function Shooter.canShoot()
    if Board.rowAnimation then return false end
    return Shooter.available and BlockQueue.available and Shooter.charac.state ~= "walk"
end

function Shooter.draw()

    -- aim
    -- local x,y = (Shooter.topleft + (Shooter.ipos-Vector(1,1)) ^ Board.cellSize):unpack()

    love.graphics.setColor(255,255,255)
    love.graphics.draw(Shooter.aimline.sheet.batch)

    love.graphics.draw(Shooter.charac.sheet.batch)


    if Shooter.charge_visible then
        local ndx = BlockQueue.first()
        local xx, yy = (Shooter.topleft + (Shooter.ipos-Vector(1,1)) ^ Board.cellSize):unpack()
        yy = yy - 8
        xx = Shooter.walk.pos - 16  + Shooter.walk.wobble
        if ndx == 0 then
            -- rainbow
            local frame = BlockQueue.anim.frame % 8 + 1
            love.graphics.draw(BlockQueue.sheet.batch_shoot:getTexture(),
                BlockQueue.sheet.sprites.rainbow_capsule[frame], xx, yy)

        else
            local frame = BlockQueue.anim.frame % 4 + 1
            love.graphics.draw(BlockQueue.sheet.batch_shoot:getTexture(),
                BlockQueue.sheet.sprites.capsules[ndx][frame], xx, yy)
        end
    end

    if gameState == "playing" and Shooter.shoot_wait_timer <= 3 then
        local time_display = math.ceil(Shooter.shoot_wait_timer)
        local xofs = 16
        if Shooter.walk.orientation < 0 then
            xofs = -8
        end
        Graphics.print(tostring(time_display), Shooter.walk.pos - 16 + xofs, Shooter.topleft.y - 24)
    end

end