Graphics = Graphics or {}

function Graphics.init()
    Graphics.bg = love.graphics.newImage("img/BG1.png")
    Graphics.bgOrigin = Vector(24,0)

    Graphics.platform = love.graphics.newImage("img/platform.png")
    Graphics.platformOrigin = Vector(27,381)

    Graphics.hue = 0

    Graphics.font = love.graphics.newImageFont("img/puyofont_contour.png","ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.:!?+-_ ")

    Graphics.button_color = {237, 237, 190}
    Graphics.button_border = {48, 32, 32}
    Graphics.button_TL = Vector(140,360)
    Graphics.go_width = Graphics.font:getWidth("1 GAME OVER 1")

    -- intro
    Graphics.logo_left = love.graphics.newImage("img/puyo_txt.png")
    Graphics.logo_right = love.graphics.newImage("img/mouse_txt.png")
    Graphics.introPercent = 0

    -- outro
    Graphics.desatShader = love.graphics.newShader([[
        uniform float amount;
        vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
        {
            vec3 grayXfer = vec3(0.3, 0.59, 0.11);
            vec4 texcolor = Texel(texture, texture_coords) * color;
            vec3 gray = vec3(dot(grayXfer, texcolor.xyz));
            return vec4(mix(texcolor.xyz, gray, amount), texcolor.w);
        }
    ]])
end

function Graphics.reset()
    -- Graphics.show_help = true
    Graphics.show_help = false
end

function Graphics.update(dt)
    if Graphics.show_help then
        Graphics.hue = (Graphics.hue + dt*400) % 360
    end
end

function Graphics.drawBg()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(Graphics.bg, -Graphics.bgOrigin.x, -Graphics.bgOrigin.y)
    love.graphics.draw(Graphics.platform, Graphics.platformOrigin.x, Graphics.platformOrigin.y)
end

function Graphics.drawFg()
    if Graphics.show_help then
        -- love.graphics.setColor(255,255,255)
        love.graphics.setColor(hsl2rgb(Graphics.hue, 100, 90, 128 + 64))
        local xx, yy = (Shooter.topleft + (Shooter.ipos-Vector(1,1)) ^ Board.cellSize):unpack()
        yy = yy + 16
        xx = Shooter.walk.pos - 8  + Shooter.walk.wobble

        -- shoot display
        love.graphics.circle("fill", xx, yy, 16, 16)
        love.graphics.rectangle("fill", xx-4, yy - 32 - 16, 8, 24)
        love.graphics.polygon("fill", xx-16, yy-32-16, xx, yy-64-8, xx+16, yy-32-16)

        -- drag display
        local yy = yy - 148
        love.graphics.circle("fill", xx, yy, 16, 16)
        love.graphics.rectangle("fill", xx-32-16, yy-4, 24, 8)
        love.graphics.rectangle("fill", xx+32+16-24, yy-4, 24, 8)
        love.graphics.polygon("fill", xx-32-16, yy-16, xx-64-8, yy, xx-32-16, yy+16)
        love.graphics.polygon("fill", xx+32+16, yy-16, xx+64+8, yy, xx+32+16, yy+16)
    end

    if gameState == "intro" and Graphics.introPercent > 0 then
        local y = 180
        love.graphics.setColor(255,255,255)
        local x0 = screenSize.x*0.45
        local w = Graphics.logo_left:getWidth()
        local x = math.floor(x0*Graphics.introPercent - w)
        love.graphics.draw(Graphics.logo_left,x,y)
        love.graphics.draw(Graphics.logo_right,
            math.floor(screenSize.x-(screenSize.x-x0)*Graphics.introPercent),
            y+3)

        if Graphics.introPercent >= 1 then
            love.graphics.setColor(255,255,255)
            Graphics.roundedRect(Graphics.button_TL.x, Graphics.button_TL.y, 60, 30, 6)
            love.graphics.setColor(255,255,255)
            Graphics.print("START", Graphics.button_TL.x + 10, Graphics.button_TL.y + 10)

            if Score.hiscore > 0 then
                Graphics.printc("TOP: "..tostring(Score.hiscore), screenSize.x*0.5, 210)
            end
        end
    elseif gameState == "gameover" then
        local x,y = screenSize.x*0.5, 120

        local scoretxt = "SCORE: "..tostring(Score.score)
        local toptxt = "TOP: "..tostring(Score.hiscore)
        local w = math.max(Graphics.go_width, Graphics.font:getWidth(scoretxt.."AB"))
        local h = Graphics.font:getHeight()
        local rh = h*3+12
        if Score.hiscore > 0 then
            rh = rh + h + 2
            w = math.max(w, Graphics.font:getWidth(toptxt.."AB"))
        end
        love.graphics.setColor(128,128,128,Graphics.gameover_opacity*128)
        love.graphics.rectangle("fill",0,0,screenSize.x, screenSize.y)

        love.graphics.setColor(255,255,255, Graphics.gameover_opacity*255)
        Graphics.roundedRect(x-w*0.5, y-10, w*1, rh, 6)

        love.graphics.setColor(255,255,255, Graphics.gameover_opacity*255)
        Graphics.printc("GAME OVER",x,y)
        Graphics.printc(scoretxt,x,y+2*h+1)
        if Score.hiscore > 0 then
            Graphics.printc(toptxt, x, y+3*h+2)
        end

        if Graphics.gameover_opacity >= 1 then
            love.graphics.setColor(255,255,255,255)
            Graphics.roundedRect(Graphics.button_TL.x, Graphics.button_TL.y, 88, 30, 6)
            love.graphics.setColor(255,255,255)
            Graphics.print("NEW GAME", Graphics.button_TL.x + 10, Graphics.button_TL.y + 10)
        end
    end
end

function Graphics.roundedRect(x,y,w,h,r)
    local _,_,_,_a = love.graphics.getColor()

    -- border
    local _r,_g,_b = unpack(Graphics.button_border)
    love.graphics.setColor(_r,_g,_b,_a)
    love.graphics.arc( "line", "open", x+r, y+r, r+0.5, math.pi, 3*math.pi/2, r )
    love.graphics.arc( "line", "open", x+w-r, y+r, r+0.5, 3*math.pi/2, 2*math.pi, r )
    love.graphics.arc( "line", "open", x+r, y+h-r, r+0.5, math.pi/2, math.pi, r )
    love.graphics.arc( "line", "open", x+w-r, y+h-r, r+0.5, 0, math.pi/2, r )
    love.graphics.line(x+r,y-0.5,x+w-r,y-0.5)
    love.graphics.line(x+r,y+h+0.5,x+w-r,y+h+0.5)
    love.graphics.line(x-0.5,y+r,x-0.5,y+h-r)
    love.graphics.line(x+w+0.5,y+r,x+w+0.5,y+h-r)

    -- inside
    local _r,_g,_b = unpack(Graphics.button_color)
    love.graphics.setColor(_r,_g,_b, _a)
    love.graphics.rectangle("fill", x,y+r,w,h-2*r)
    love.graphics.rectangle("fill", x+r,y,w-2*r,r)
    love.graphics.rectangle("fill", x+r,y+h-r,w-2*r,r)
    love.graphics.arc( "fill", x+r, y+r, r, math.pi, 3*math.pi/2, r )
    love.graphics.arc( "fill", x+w-r, y+r, r, 3*math.pi/2, 2*math.pi, r )
    love.graphics.arc( "fill", x+r, y+h-r, r, math.pi/2, math.pi, r )
    love.graphics.arc( "fill", x+w-r, y+h-r, r, 0, math.pi/2, r )

end

function Graphics.print(txt,x,y)
    love.graphics.setFont(Graphics.font)
    love.graphics.print(string.upper(txt), x, y)
end

function Graphics.printc(txt,x,y)
    txt = string.upper(txt)
    local w,h = Graphics.font:getWidth(txt),Graphics.font:getHeight(txt)
    love.graphics.setFont(Graphics.font)
    love.graphics.print(txt, math.floor(x - w*0.5), math.floor(y-h*0.5))
end
