BlockQueue = BlockQueue or {}

function BlockQueue.init()
    BlockQueue.nlist = 12
    BlockQueue.topleft = Vector(25,22)
    BlockQueue.initGraphics()
    BlockQueue.offset = 0
    BlockQueue.available = true
end

function BlockQueue.reset()
    BlockQueue.available = true
    BlockQueue.list = {}
    for i=1,BlockQueue.nlist do
        BlockQueue.list[i] = BlockQueue.getRandomColor()
    end
    BlockQueue.chamber = { BlockQueue.getRandomColor() }
    BlockQueue.launchAnim()
end

function BlockQueue.initGraphics()
    BlockQueue.sheet = {}
    local img = love.graphics.newImage("img/sheet_shots.png")
    local w,h = img:getWidth(), img:getHeight()
    BlockQueue.sheet.sprites = {}
    BlockQueue.sheet.batch = love.graphics.newSpriteBatch(img, 12*24*8, "static")
    BlockQueue.sheet.batch_shoot = love.graphics.newSpriteBatch(img, 12*24*8, "static")
    BlockQueue.sheet.batch_chamber = love.graphics.newSpriteBatch(img, 12*24*8, "static")

    BlockQueue.sheet.sprites.rainbow_shoot = {}
    BlockQueue.sheet.sprites.rainbow_capsule = {}
    for ix=0,7 do
        table.insert(BlockQueue.sheet.sprites.rainbow_shoot,
            love.graphics.newQuad(ix*16,0,16,16,w,h))
        table.insert(BlockQueue.sheet.sprites.rainbow_capsule,
            love.graphics.newQuad(ix*16,16,16,16,w,h))
    end
    BlockQueue.sheet.sprites.capsules = {}
    -- for ix=0,7 do
    for iy=2,7 do
        local ii = iy-1
        BlockQueue.sheet.sprites.capsules[ii] = {}
        for ix = 0,3 do
            table.insert(BlockQueue.sheet.sprites.capsules[ii],
                love.graphics.newQuad(ix*16,iy*16,16,16,w,h))
        end
    end

    BlockQueue.setCapsuleDirty()
end

function BlockQueue.launchAnim()
    BlockQueue.anim = {}
    CreateRepeatAnimation(8, 0.095, BlockQueue.setCapsuleDirty, BlockQueue.anim):start()
    BlockQueue.phase = 0
    BlockQueue.freq = 8
    BlockQueue.wavelength = 0.75
end

function BlockQueue.setCapsuleDirty()
    BlockQueue.sheet.capsule_dirty = true
end

function BlockQueue.update(dt)
    -- if BlockQueue.sheet.capsule_dirty then
        -- BlockQueue.sheet.capsule_dirty = false

        BlockQueue.phase = (BlockQueue.phase + dt * BlockQueue.freq) % (2*math.pi)

        BlockQueue.sheet.batch:clear()

        for i=1,#BlockQueue.list do
            local ndx = BlockQueue.list[i]
            local xx,yy =  BlockQueue.topleft.x + (i-1)*Board.cellSize.x, BlockQueue.topleft.y
            local vofs = 2 * math.sin(BlockQueue.phase - i*BlockQueue.wavelength)
            if ndx == 0 then
                -- rainbow
                local frame = (i + BlockQueue.anim.frame) % 8 + 1
                BlockQueue.sheet.batch:add(BlockQueue.sheet.sprites.rainbow_capsule[frame],
                    xx,yy + vofs)
            else
                local frame = ((i + BlockQueue.anim.frame)%4)+1
                BlockQueue.sheet.batch:add(BlockQueue.sheet.sprites.capsules[ndx][frame],
                    xx,yy + vofs)

            end
        end
    -- end
end

function BlockQueue.first()
    return BlockQueue.chamber[1]
end

function BlockQueue.getRandomColor()
    local rainbow_prop = math.min(0.8, 0.2 + 0.6/31*(Board.level-1))
    if math.random() < rainbow_prop then
        return 0;
    else
        return Board.getRandomColor()
    end
end

function BlockQueue.iterate()
    table.insert(BlockQueue.chamber, BlockQueue.list[1])
    table.remove(BlockQueue.list, 1)
    table.insert(BlockQueue.list, BlockQueue.getRandomColor())
    BlockQueue.setCapsuleDirty()

    -- BlockQueue.available = false
    BlockQueue.offset = 16
    local T = 0.2
    -- shift animation
    Timers.create(T):withUpdate(
        function(elapsed)
            BlockQueue.offset = 1 - elapsed/T
        end)
    :andThen(function()
        BlockQueue.offset = 0
        -- BlockQueue.available = true
        end)
    :start()
    return BlockQueue.first()
end

function BlockQueue.block()
    BlockQueue.available = false
end

function BlockQueue.consume()
    -- table.remove(BlockQueue.chamber, 1)
    -- BlockQueue.available = true
    table.remove(BlockQueue.chamber, 1)
    local consumedBlock = BlockQueue.chamber[1]
    local T = 0.7

    ---- second order equation
    -- y = y0 + t*A + t*t*B
    -- y1 = y0 + fA + f*fB
    -- y2 = y0 + A + B
    local y0 = BlockQueue.topleft.y
    local y1 = 16
    local y2 = Shooter.topleft.y - 24
    local f = 0.5

    local num = 1/(f*(f-1))
    local A = (f*f*(y2-y0) + y0 - y1)*num
    local B = ((f-1)*y0-f*y2+y1)*num

    Timers.create(T):withUpdate(
        function(elapsed)
            BlockQueue.sheet.batch_chamber:clear()
            local f = elapsed/T
            local xto = (Shooter.walk.pos - 16  + Shooter.walk.wobble)
            local xfrom = BlockQueue.topleft.x
            local xx = xfrom + f * (xto-xfrom)
            local yy = y0 + f * A + f*f*B

            if consumedBlock == 0 then
                -- rainbow
                local frame = BlockQueue.anim.frame % 8 + 1
                BlockQueue.sheet.batch_chamber:add(
                    BlockQueue.sheet.sprites.rainbow_capsule[frame], xx, yy)
            else
                local frame = BlockQueue.anim.frame % 4 + 1
                BlockQueue.sheet.batch_chamber:add(
                    BlockQueue.sheet.sprites.capsules[consumedBlock][frame], xx, yy)
            end
        end)
    :andThen(function()
        BlockQueue.sheet.batch_chamber:clear()
        BlockQueue.available = true
        Shooter.charge_visible = true
    end)
    :start()

end

function BlockQueue.draw()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(BlockQueue.sheet.batch, BlockQueue.offset*16, 0)
    love.graphics.draw(BlockQueue.sheet.batch_shoot)
    love.graphics.draw(BlockQueue.sheet.batch_chamber)
end