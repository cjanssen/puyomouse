Particles = Particles or {}

function Particles.init()
    Particles.list = {}
    Particles._dirty = true

    do
        -- batch
        local img = love.graphics.newImage("img/star.png")
        local w,h = img:getWidth(), img:getHeight()
        Particles.batch = love.graphics.newSpriteBatch(img, 1000, "static")
        Particles.sprite = love.graphics.newQuad(0,0,w,h,w,h)
        Particles.size = Vector(w,h)
    end

    Particles.speed = 30
    Particles.gravity = 70
end

function Particles.reset()
    Particles.list = {}
    Particles._dirty = true
end

function Particles.update(dt)
    if #Particles.list > 0 then
        Particles._dirty = true
    end

    for i=#Particles.list,1,-1 do
        local particle = Particles.list[i]
        particle.life = particle.life - dt
        if particle.life <= 0 then
            table.remove(Particles.list, i)
        else
            particle.pos = particle.pos + particle.vel * dt
            particle.angle = particle.angle + particle.angvel * dt
            particle.opacity = math.max(particle.life / particle.expectancy, 0)
            -- gravity
            particle.vel.y = particle.vel.y + Particles.gravity * dt
            particle.color = { hsv2rgb(particle.hue, 100, 100) }
            particle.hue = (particle.hue + 300*dt)%360
        end
    end


    if Particles._dirty then
        Particles.batch:clear()
        for i=1,#Particles.list do
            local particle = Particles.list[i]
            Particles.batch:setColor(particle.color[1],particle.color[2],particle.color[3],particle.opacity*255)
            Particles.batch:add(Particles.sprite, particle.pos.x, particle.pos.y, particle.angle, 1, 1, Particles.size.x*0.5, Particles.size.y*0.5)
        end
    end
end

function Particles.spawn(pos, vel)
    local life = (math.random()*2 + 1)
    table.insert(Particles.list, {
        pos = pos:copy(),
        vel = vel:rot((math.random()-0.5) * 0.8),
        angle = math.random()*math.pi*2,
        angvel = (math.random()-0.5)*math.pi*2*2,
        life = life,
        expectancy = life,
        opacity = 1,
        -- color = { hsv2rgb(math.random(360),100,100) }
        hue = math.random(360)
    })
end

function Particles.draw()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(Particles.batch)
end